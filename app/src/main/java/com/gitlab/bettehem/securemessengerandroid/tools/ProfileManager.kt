package com.gitlab.bettehem.securemessengerandroid.tools

import android.content.Context
import com.github.bettehem.androidtools.Preferences
import java.util.*

//a class that manages profiles
class ProfileManager {

    /**
     * checks if user has created a profile
     * @return returns true if a profile has been created
     */
    fun profileCreated(context: Context?) = Preferences.loadBoolean(context, "profileCreated")

    /**
     * gets the current user profile
     * @param context context
     * @return returns the current user profile
     */
    fun getCurrentProfile(context: Context?): UserProfile {
        val currentProfileData = Preferences.loadStringArray(context, "currentProfile")
        val userProfile = UserProfile("", "")
        return if (currentProfileData.size <= 1){
            userProfile
        }else{
            getProfile(context, currentProfileData[0], currentProfileData[1])
        }
    }

    /**
     * sets the current user profile
     * @param context context
     * @param userProfile user profile
     */
    fun setCurrentProfile(context: Context?, userProfile: UserProfile) {
        Preferences.saveStringArray(context, "currentProfile", arrayOf(userProfile.username, userProfile.userID))
    }

    /**
     * get a user profile based on username and id
     * @param context context
     * @param username username to get profile data from
     * @param userID user id to get profile data from
     * @return returns a UserProfile object based on the given username and id
     */
    fun getProfile(context: Context?, username: String, userID: String): UserProfile {
        var userProfile = UserProfile("", "")
        for (profile in getUserProfileList(context)) {
            if (profile.username == username && profile.userID == userID) {
                userProfile = profile
            }
        }
        return userProfile
    }

    /**
     * generates a random user ID
     * @return returns a string which contains the userID
     */
    fun generateUserID(): String{
        fun ClosedRange<Int>.random() = Random().nextInt(endInclusive - start) + start
        var userID = ""
        for (num in (0..4).map { (0..10).random() }){
            userID += num.toString()
        }
        return userID
    }

    /**
     * saves a new  user profile
     * @param context context
     * @param userProfile an object that contains username, id and status
     */
    fun saveNewProfile(context: Context?, userProfile: UserProfile) {
        //filename for the profile
        val profileFilename = userProfile.username + "-" + userProfile.userID

        //save username to UserProfile file
        Preferences.saveString(context, "username", userProfile.username, profileFilename)
        Preferences.saveString(context, "userId", userProfile.userID, profileFilename)
        Preferences.saveString(context, "status", userProfile.status, profileFilename)

        //change profileCreated to true
        Preferences.saveBoolean(context, "profileCreated", true)
        //save new profile amount
        setProfileAmount(context, getProfileAmount(context) + 1)
        //save username to profileList
        addUserToProfileList(context, userProfile.username, userProfile.userID)

        setCurrentProfile(context, userProfile)
    }

    /**
     * updates profile information
     * @param context context
     * @param userProfile user profile that should be updated
     * @param newUsername optionally set new username
     * @return returns the updated UserProfile
     */
    fun updateProfile(context: Context?, userProfile: UserProfile, newUsername: String = userProfile.username): UserProfile {

        val updatedProfile = userProfile

        //loop through existing usernames
        for (username in getUserProfileList(context)) {
            //check if username matches userProfile.username
            if (username.username == userProfile.username) {

                //check if newUsername is different from original username
                var updatedUsername = username.username
                //check if user updated username
                if (newUsername != userProfile.username) {
                    updatedUsername = newUsername
                    //delete old UserProfile file
                    Preferences.deleteFile(context, username.username + "-" + username.userID, "xml")

                    //update profile list
                    //first remove old profile from list
                    deleteProfile(context, userProfile)
                    saveNewProfile(context, UserProfile(updatedUsername, userProfile.userID, userProfile.status))

                    //update newUserProfile to have the new username
                    updatedProfile.username = newUsername
                }else{
                    //save updated data
                    Preferences.saveString(context, "username", updatedUsername, userProfile.toString())
                    Preferences.saveString(context, "userId", userProfile.userID, userProfile.toString())
                    Preferences.saveString(context, "status", userProfile.status, userProfile.toString())
                }
            }
        }

        return updatedProfile
    }

    /**
     * deletes a profile
     * @param context context
     * @param userProfile user profile that needs deletion
     */
    fun deleteProfile(context: Context?, userProfile: UserProfile) {
        //get profile list
        val profiles = ArrayList<UserProfile>()

        //loop trough profile list and delete profile if exists
        for (profile in getUserProfileList(context)) {
            if (profile.username != userProfile.username && profile.userID != userProfile.userID) {
                profiles.add(profile)
            }
        }
        //set profile amount to be 1 less than previously
        setProfileAmount(context, getProfileAmount(context) - 1)

        //remove item from profileList
        removeFromProfileList(context, userProfile)

        //delete profile file
        Preferences.deleteFile(context, userProfile.username + "-" + userProfile.userID, "xml")

        //if user has more profiles, set active profile to the next one in the list
        if (getProfileAmount(context) > 0) {
            setCurrentProfile(context, getUserProfileList(context)[0])
        } else {
            Preferences.saveBoolean(context, "profileCreated", false)
            setCurrentProfile(context, UserProfile("", ""))
        }
    }

    /**
     * deletes all profiles
     * @param context context
     */
    fun deleteAllProfiles(context: Context?) {
        //loop through profile list and delete each profile
        for (profile in getUserProfileList(context)) {
            deleteProfile(context, profile)
        }
        //set profileCreated to false
        Preferences.saveBoolean(context, "profileCreated", false)
    }


    /**
     * get a list of existing user profiles
     * @param context context
     * @return returns an ArrayList which contains UserProfile objects
     */
    fun getUserProfileList(context: Context?): ArrayList<UserProfile> {
        val profileList = ArrayList<UserProfile>()
        for (profileName in Preferences.loadStringArray(context, "profileList")) {
            val username = Preferences.loadString(context, "username", profileName)
            val userID = Preferences.loadString(context, "userId", profileName)
            val status = Preferences.loadString(context, "status", profileName)
            if (username.isNotEmpty() && userID.isNotEmpty()) {
                profileList.add(UserProfile(username, userID, status))
            }
        }
        return profileList
    }


    /**
     * adds username to profileList
     * @param context context
     * @param username username to add to list of profiles
     * @param userID user's id
     */
    private fun addUserToProfileList(context: Context?, username: String, userID: String) {
        //get list of usernames
        val usernames = getUserProfileList(context)
        //add new username to list of UsernameProfiles
        usernames.add(UserProfile(username, userID))


        //add profiles to profileList
        val profiles = ArrayList<String>()
        usernames.all { it -> profiles.add(it.username + "-" + it.userID) }
        Preferences.saveStringArray(context, "profileList", profiles.toTypedArray())

    }

    /**
     * deletes an item from profileList
     * @param context context
     * @param userProfile user profile that should be removed
     */
    private fun removeFromProfileList(context: Context?, userProfile: UserProfile) {
        //get list of usernames
        val usernames = getUserProfileList(context)
        val profiles = ArrayList<String>()
        for (profile in usernames){
            if (profile.username != userProfile.username && profile.userID != userProfile.userID){
                profiles.add(profile.toString())
            }
        }
        setProfileList(context, profiles.toTypedArray())
    }

    /**
     * sets profileList to a provided list
     * @param context context
     * @param profileList a list of profiles
     */
    private fun setProfileList(context: Context?, profileList: Array<String>) {
        Preferences.saveStringArray(context, "profileList", profileList)
    }

    /**
     * gets the amount of profiles
     * @param context context
     * @return returns the amount of profiles
     */
    private fun getProfileAmount(context: Context?) = Preferences.loadInt(context, "profileAmount")

    /**
     * sets the amount of profiles
     * @param context context
     * @param profileAmount new amount of profiles
     */
    private fun setProfileAmount(context: Context?, profileAmount: Int) = Preferences.saveInt(context, "profileAmount", profileAmount)


    class UserProfile(var username: String, val userID: String, var status: String = "") {
        override fun toString(): String {
            return "$username-$userID"
        }
    }
}