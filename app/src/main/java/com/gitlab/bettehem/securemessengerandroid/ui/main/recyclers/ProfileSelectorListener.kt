package com.gitlab.bettehem.securemessengerandroid.ui.main.recyclers

import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager

interface ProfileSelectorListener{
    fun onProfileSelected(userProfile: ProfileManager.UserProfile)
    fun onProfileItemChecked(userProfile: ProfileManager.UserProfile)
    fun onProfileItemUnchecked(userProfile: ProfileManager.UserProfile)
}