package com.gitlab.bettehem.securemessengerandroid.ui.main


import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.gitlab.bettehem.securemessengerandroid.R
import com.gitlab.bettehem.securemessengerandroid.ui.main.viewmodels.ProfileManagerViewModel
import kotlinx.android.synthetic.main.new_profile_layout.*

//this fragment handles profile creation
class ProfileCreatorFragment : Fragment() {

    private lateinit var viewModel: ProfileManagerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.new_profile_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //attach ViewModel
        viewModel = ViewModelProviders.of(activity!!).get(ProfileManagerViewModel::class.java)

        //setup buttons
        buttons()
    }


    //setup buttons
    private fun buttons(){
        saveProfileButton.setOnClickListener {
            //check if user has typed a username and only then attempt to save a profile
            if (newUsernameEditText.text!!.isNotEmpty()){
                //TODO: subscribe to firebase messaging topic
                //TODO: don't accept spaces in usernames
                //save profile
                viewModel.saveNewProfile(newUsernameEditText.text.toString())
                findNavController().navigateUp()
            }
        }
    }

}
