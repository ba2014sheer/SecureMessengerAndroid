package com.gitlab.bettehem.securemessengerandroid

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.AppCompatTextView
import android.view.MenuItem
import android.view.View
import androidx.navigation.findNavController
import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager
import com.gitlab.bettehem.securemessengerandroid.ui.main.viewmodels.ProfileManagerViewModel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import kotlinx.android.synthetic.main.main_activity.*

//This is the main activity of the app
class MainActivity : AppCompatActivity() {

    private lateinit var profileManagerViewModel: ProfileManagerViewModel
    private lateinit var navigationView: NavigationView
    private lateinit var drawerLayout: DrawerLayout

    //called when the activity is created
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //set to display the main_activity layout
        setContentView(R.layout.main_activity)

        //set toolbar
        setSupportActionBar(mainToolbar)

        //check for google play services.
        //if not available, direct user to install
        if (!googlePlayServicesAvailable(this)){
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
        }


        //attach ViewModel
        profileManagerViewModel = ViewModelProviders.of(this).get(ProfileManagerViewModel::class.java)

        //setup navigation drawer
        navDrawerSetup()
    }

    //called when the activity is resumed
    override fun onResume() {
        super.onResume()
        //check for google play services.
        //if not available, direct user to install
        if (!googlePlayServicesAvailable(this)){
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
        }
    }

    //called when the back button is pressed
    override fun onBackPressed() {
        //close navigation drawer if it's open
        if (drawerLayout.isDrawerVisible(GravityCompat.START)){
            //close drawer
            drawerLayout.closeDrawers()
        }else{
            //do normal back press action(s)
            super.onBackPressed()
        }
    }

    /**
     * checks if google play services is available
     * @return returns true if google play services is available and false otherwise
     */
    private fun googlePlayServicesAvailable(context: Context): Boolean{
        val googleApiAvailability = GoogleApiAvailability.getInstance()

        return googleApiAvailability.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS
    }

    //tells which navController should navigate up
    override fun onSupportNavigateUp() = findNavController(R.id.nav_main_fragment).navigateUp()

    //setup navigation drawer
    private fun navDrawerSetup(){
        drawerLayout = findViewById(R.id.main_drawer_layout)
        val toggle = ActionBarDrawerToggle(this, drawerLayout, findViewById(R.id.mainToolbar), R.string.openNavDrawerText, R.string.closeNavDrawerText)
        toggle.syncState()

        navigationView = findViewById(R.id.nav_view)

        //get header view
        val headerView = navigationView.getHeaderView(0)
        //assign header's texts
        val navHeaderUsernameText = headerView.findViewById<AppCompatTextView>(R.id.navDrawerUsernameTextView)
        val navHeaderUserIdText = headerView.findViewById<AppCompatTextView>(R.id.navDrawerIdTextView)
        val navHeaderStatusText = headerView.findViewById<AppCompatTextView>(R.id.navDrawerStatusTextView)

        //update nav drawer header with current profile's details when observer get's called
        val currentProfileObserver: Observer<ProfileManager.UserProfile> = Observer { profile ->
            //update header with current profile's details if it's not empty
            if (profile!!.username.isNotEmpty() && profile.userID.isNotEmpty()){
                //update header
                navHeaderUsernameText.text = profile.username
                navHeaderUserIdText.text = profile.userID
                //also update status and make it visible if current profile contains one
                if (profile.status.isNotEmpty()){
                    navHeaderStatusText.text = profile.status
                    navHeaderStatusText.visibility = View.VISIBLE
                }else{
                    navHeaderStatusText.text = ""
                    navHeaderStatusText.visibility = View.GONE
                }
            }else{
                //set header to show noUserProfileText
                navHeaderUsernameText.text = getString(R.string.noUserProfileText)
                navHeaderUserIdText.text = ""
                navHeaderStatusText.text = ""
                navHeaderStatusText.visibility = View.GONE
            }
        }
        //start observing
        profileManagerViewModel.getCurrentProfile().observe(this, currentProfileObserver)

        val updatedProfileObserver: Observer<ProfileManager.UserProfile> = Observer { profile ->
            //check if current profile is the same as the updated profile. If so, update header
            val currentProfile = ProfileManager().getCurrentProfile(this)
            if (profile!!.username == currentProfile.username && profile.userID == currentProfile.userID){
                if (profile.username.isNotEmpty() && profile.userID.isNotEmpty()){
                    //update header
                    navHeaderUsernameText.text = profile.username
                    navHeaderUserIdText.text = profile.userID
                    //also update status and make it visible if current profile contains one
                    if (profile.status.isNotEmpty()){
                        navHeaderStatusText.text = profile.status
                        navHeaderStatusText.visibility = View.VISIBLE
                    }else{
                        navHeaderStatusText.text = ""
                        navHeaderStatusText.visibility = View.GONE
                    }
                }else{
                    //set header to show noUserProfileText
                    navHeaderUsernameText.text = getString(R.string.noUserProfileText)
                    navHeaderUserIdText.text = ""
                    navHeaderStatusText.text = ""
                    navHeaderStatusText.visibility = View.GONE
                }
            }
        }
        profileManagerViewModel.getUpdatedProfile().observe(this, updatedProfileObserver)



        //handle navigation drawer item clicks here
        navigationView.setNavigationItemSelectedListener { menuItem: MenuItem ->
            //check which item was selected
            when (menuItem.itemId){

                R.id.nav_profile_selector -> {
                    //close the navigation drawer
                    drawerLayout.closeDrawers()

                    if (!menuItem.isChecked){
                        //navigate to the profile selector fragment
                        findNavController(R.id.nav_main_fragment).navigate(R.id.toProfileSelectorFragment)
                    }

                    //set item to be checked
                    menuItem.isChecked = true
                }

            }

            //return true to indicate that the item click has been handled
            true
        }
    }

}
