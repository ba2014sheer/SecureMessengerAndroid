package com.gitlab.bettehem.securemessengerandroid.tools

import java.io.DataOutputStream

//manages root access stuff
class RootManager{

    /**
     * checks checks for root access
     * @return returns true if rooted, false otherwise
     */
    fun isRooted(): Boolean{
        return try {
            val p: Process = Runtime.getRuntime().exec("su")
            val os = DataOutputStream(p.outputStream)
            os.writeBytes("echo \"root test\" > /system/sd/temp.txt\"")
            os.writeBytes("exit\n")
            os.flush()
            p.waitFor()
            p.exitValue() != 255
        }catch (exception: Exception){
            exception.printStackTrace()
            false
        }
    }
}