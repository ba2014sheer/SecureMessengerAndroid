package com.gitlab.bettehem.securemessengerandroid.ui.main


import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.gitlab.bettehem.securemessengerandroid.R
import com.gitlab.bettehem.securemessengerandroid.tools.ProfileManager
import com.gitlab.bettehem.securemessengerandroid.ui.main.viewmodels.ProfileManagerViewModel
import kotlinx.android.synthetic.main.fragment_profile_editor.*

class ProfileEditorFragment : Fragment(), View.OnClickListener {

    private lateinit var userProfile: ProfileManager.UserProfile
    private lateinit var profileManagerViewModel: ProfileManagerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_editor, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //attach ViewModel
        profileManagerViewModel = ViewModelProviders.of(activity!!).get(ProfileManagerViewModel::class.java)

        //set user profile
        userProfile = ProfileManager().getProfile(activity, ProfileEditorFragmentArgs.fromBundle(arguments!!).username, ProfileEditorFragmentArgs.fromBundle(arguments!!).userID)

        //set to display correct data
        profileEditorUsernameEditText.setText(userProfile.username)
        profileEditorUserIdTextView.text = resources.getString(R.string.userIdText, userProfile.userID)
        profileEditorStatusEditText.setText(userProfile.status)

        //setup buttons
        buttons()
    }

    //setup buttons
    private fun buttons(){
        //set onClickListeners
        profileEditorSetAsActiveProfileButton.setOnClickListener(this)
        profileEditorSaveProfileButton.setOnClickListener(this)
    }

    //handle clicks
    override fun onClick(v: View?) {
        //check id of pressed view (for example a button), and then perform an action
        when (v?.id){

            profileEditorSetAsActiveProfileButton.id -> {
                //set as current profile
                profileManagerViewModel.setCurrentProfile(userProfile)
                findNavController().navigateUp()
            }

            profileEditorSaveProfileButton.id -> {
                //set userProfile.status as profileEditorStatusEditText's text
                userProfile.status = profileEditorStatusEditText.text.toString()
                //update profile details
                profileManagerViewModel.updateProfileDetails(userProfile, profileEditorUsernameEditText.text.toString())
                findNavController().navigateUp()
            }
        }
    }
}
