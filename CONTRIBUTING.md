Do the following you want to contribute code to this project:<br>
1. Fork this repo<br>
2. Make new branch and make changes.<br>
3. Make a Merge Request to this project<br>
Your code changes will then be reviewed and then possibly merged to the main branch.<br>
<br>
<br>
<br>
You can also contribute by testing the app and then submitting issues if you find bugs or other problems in the app.<br>